# Example of Scraping Ticker Data from Coinmarketcap #

- API documentation: https://coinmarketcap.com/api/
- Implemented in Python Scrapy
- Scraped data are stored in MySQL 

## 1. Prerequisites ##

- Python packages:
    + Scrapy, Sqlalchemy
- A running MySQL server

## 2. How to use ##

Fill in your mysql database information in `coinmarketcap_scraper/database/connection.py`.

In the root of the project directory, run the following command:

```bash
scrapy crawl ticker_spider
```

Log will be save in `./ticker_spider.log` file.

## 3. Outcome ##

![](./ticker_data_in_mysql.jpg)