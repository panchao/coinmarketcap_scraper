# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import sys
import uuid
from sqlalchemy.exc import IntegrityError, DataError

from .context import database

from database.connection import Session
from database.models import Ticker
from spiders.ticker_spider import SingleCoinTickerItem


class CoinmarketcapScraperPipeline(object):
    def process_item(self, item, spider):
        return item

class AddItemToMysqlPipeline(object):
    def process_item(self, item, spider):
        """create a new SQL Alchemy object and add to the db session"""
        if type(item).__name__ == 'SingleCoinTickerItem':
            row_id = uuid.uuid4().hex  # Unique record id in the scraped dataset (UUID hex string)
            record = Ticker(uuid = row_id,
                            id = item['id'],
                            name = item['name'],
                            symbol = item['symbol'],
                            rank = item['rank'],
                            price_usd = item['price_usd'],
                            price_btc = item['price_btc'],
                            volume_24h_usd = item['volume_24h_usd'],
                            market_cap_usd = item['market_cap_usd'],
                            available_supply = item['available_supply'],
                            total_supply = item['total_supply'],
                            max_supply = item['max_supply'],
                            percent_change_1h = item['percent_change_1h'],
                            percent_change_24h = item['percent_change_24h'],
                            percent_change_7d = item['percent_change_7d'],
                            last_updated = item['last_updated'],
                            last_updated_converted = item['last_updated_converted'],
                            price_cny = item['price_cny'],
                            volume_24h_cny = item['volume_24h_cny'],
                            market_cap_cny = item['market_cap_cny'])
        else:
            raise TypeError('Unknown item type %s' % type(item))

        try:
            Session.add(record)
            Session.commit()
            return item 
        except IntegrityError as e: 
            spider.logger.error("IntegrityError: \n %s" % sys.exc_info()[0])
            spider.logger.error(e)
            Session.rollback()
        except DataError as e:  # e.g. _mysql_exceptions.DataError: (1406, "Data too long for column 'mandator_name' at row 1")
            spider.logger.error("DataError: \n %s" % sys.exc_info()[0])
            spider.logger.error(e)
            Session.rollback()
        except:
            spider.logger.error("Unexpected error:  \n %s" % sys.exc_info()[0])
            raise