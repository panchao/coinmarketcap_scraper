# -*- coding: utf-8 -*-
# @Author: panc25
# @Date:   2018-01-14 16:20:18
# @Last Modified by:   panc25
# @Last Modified time: 2018-01-14 21:48:59

'''Ticker data spider by using coinmarketcap web API.

API: https://api.coinmarketcap.com/v1/ticker/?start=100&convert=EUR&limit=100

See documentation at https://coinmarketcap.com/api/
'''

import scrapy
import json

from .context import utility
# from utility.data_processing import convertEpochTimeToDatetime
import time


class SingleCoinTickerItem(scrapy.Item):
    id = scrapy.Field()         # e.g. bitcoin
    name = scrapy.Field()       # Bitcoin
    symbol = scrapy.Field()     # BTC
    rank = scrapy.Field()       # 1
    price_usd = scrapy.Field()  # 13589.2
    price_btc = scrapy.Field()  # 1.0
    volume_24h_usd = scrapy.Field()  # 10944100000.0
    market_cap_usd = scrapy.Field()  # 228338308410
    available_supply = scrapy.Field()  # 16802925.0
    total_supply = scrapy.Field()   # 21000000.0
    max_supply = scrapy.Field()
    percent_change_1h = scrapy.Field()  # 0.37
    percent_change_24h = scrapy.Field()  # -5.38
    percent_change_7d = scrapy.Field()  # -17.08
    last_updated = scrapy.Field()  # 1515964762
    last_updated_converted = scrapy.Field()  # Convert last_updated from epoch time to regular datetime
    price_cny = scrapy.Field()
    volume_24h_cny = scrapy.Field()
    market_cap_cny = scrapy.Field()


class TickerSpider(scrapy.Spider):
    name = 'ticker_spider'

    def start_requests(self):
        form_data = {}
        # Request only takes string as body so you need to
        # convert python dict to string
        request_body = json.dumps(form_data)
        header = {
            'content-type': 'application/json',
            # 'Host': 'gs.amac.org.cn',
            # 'Referer': 'http://gs.amac.org.cn/amac-infodisc/res/pof/manager/index.html',
            # 'Referer': 'http://gs.amac.org.cn/amac-infodisc/res/pof/fund/index.html',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:50.0) Gecko/20100101 Firefox/50.0',
            # 'X-Requested-With': 'XMLHttpRequest'
        }
        start_urls = ['https://api.coinmarketcap.com/v1/ticker/?limit=0&convert=CNY']
        for url in start_urls:
            yield scrapy.Request(url, method="GET", body=request_body, headers=header)

    def parse(self, response):
        """Parse the response returned by GET"""
        record_lst = json.loads(response.body_as_unicode())
        for record in record_lst:
            # create a new item
            single_coin_ticker_item = SingleCoinTickerItem()  
            # store data in the item
            single_coin_ticker_item['id']       = record['id']
            single_coin_ticker_item['name']     = record['name']
            single_coin_ticker_item['symbol']   = record['symbol']
            single_coin_ticker_item['rank']         = None if record['rank'] is None else int(record['rank'])
            single_coin_ticker_item['price_usd']    = None if record['price_usd'] is None else float(record['price_usd'])
            single_coin_ticker_item['price_btc']    = None if record['price_btc'] is None else float(record['price_btc'])
            single_coin_ticker_item['volume_24h_usd']       = None if record['24h_volume_usd'] is None else float(record['24h_volume_usd'])
            single_coin_ticker_item['market_cap_usd']       = None if record['market_cap_usd'] is None else float(record['market_cap_usd'])
            single_coin_ticker_item['available_supply']     = None if record['available_supply'] is None else float(record['available_supply'])
            single_coin_ticker_item['total_supply']         = None if record['total_supply'] is None else float(record['total_supply'])
            single_coin_ticker_item['max_supply']           = None if record['max_supply'] is None else float(record['max_supply'])
            single_coin_ticker_item['percent_change_1h']    = None if record['percent_change_1h'] is None else float(record['percent_change_1h'])
            single_coin_ticker_item['percent_change_24h']   = None if record['percent_change_24h'] is None else float(record['percent_change_24h'])
            single_coin_ticker_item['percent_change_7d']    = None if record['percent_change_7d'] is None else float(record['percent_change_7d'])
            single_coin_ticker_item['price_cny']            = None if record['price_cny'] is None else float(record['price_cny'])
            single_coin_ticker_item['volume_24h_cny']       = None if record['24h_volume_cny'] is None else float(record['24h_volume_cny'])
            single_coin_ticker_item['market_cap_cny']       = None if record['market_cap_cny'] is None else float(record['market_cap_cny'])
            single_coin_ticker_item['last_updated']         = None if record['last_updated'] is None else int(record['last_updated'])
            single_coin_ticker_item['last_updated_converted'] = None if record['last_updated'] is None \
                                                                    else time.strftime('%Y-%m-%d, %H:%M:%S', time.localtime(int(record['last_updated'])) )
            yield single_coin_ticker_item

