# -*- coding: utf-8 -*-
# @Author: panc25
# @Date:   2018-01-14 17:22:35
# @Last Modified by:   panc25
# @Last Modified time: 2018-01-14 17:23:01


'''
Use a simple (but explicit) path modification to resolve the package properly.
To give the individual import context, create a context.py file as follows.

Reference: http://docs.python-guide.org/en/latest/writing/structure/
'''

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import utility
