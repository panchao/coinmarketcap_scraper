# -*- coding: utf-8 -*-
# @Author: panc25
# @Date:   2018-01-14 17:29:27
# @Last Modified by:   panc25
# @Last Modified time: 2018-01-14 21:42:27


'''Each class is a map to a database table.

Define the classes and then create the corresponding tables in
the designated database via db connections.
'''

from sqlalchemy import Column, String, Integer, Float, DateTime
from database.connection import Base, engine


class Ticker(Base):
    __tablename__ = 'ticker'

    uuid = Column(String(32), primary_key=True)  # uuid as row id, serves as primary key in the db table
    id = Column(String(100))
    name = Column(String(100))
    symbol = Column(String(50))
    rank = Column(Integer)
    price_usd = Column(Float(asdecimal=True))
    price_btc = Column(Float(asdecimal=True))
    volume_24h_usd = Column(Float(asdecimal=True))
    market_cap_usd = Column(Float(asdecimal=True))
    available_supply = Column(Float(asdecimal=True))
    total_supply = Column(Float(asdecimal=True))
    percent_change_1h = Column(Float(asdecimal=True))
    percent_change_24h = Column(Float(asdecimal=True))
    percent_change_7d = Column(Float(asdecimal=True))
    last_updated = Column(Integer)
    last_updated_converted = Column(DateTime)
    price_cny = Column(Float(asdecimal=True))
    volume_24h_cny = Column(Float(asdecimal=True))
    market_cap_cny = Column(Float(asdecimal=True))

    def __init__(self, 
                uuid = None,
                id = None,
                name = None,
                symbol = None,
                rank = None,
                price_usd = None,
                price_btc = None,
                volume_24h_usd = None,
                market_cap_usd = None,
                available_supply = None,
                total_supply = None,
                max_supply = None,
                percent_change_1h = None,
                percent_change_24h = None,
                percent_change_7d = None,
                last_updated = None,
                last_updated_converted = None,
                price_cny = None,
                volume_24h_cny = None,
                market_cap_cny = None):
        self.uuid = uuid
        self.id = id
        self.name = name
        self.symbol = symbol
        self.rank = rank
        self.price_usd = price_usd
        self.price_btc = price_btc
        self.volume_24h_usd = volume_24h_usd
        self.market_cap_usd = market_cap_usd
        self.available_supply = available_supply
        self.total_supply = total_supply
        self.max_supply = max_supply
        self.percent_change_1h = percent_change_1h
        self.percent_change_24h = percent_change_24h
        self.percent_change_7d = percent_change_7d
        self.last_updated = last_updated
        self.last_updated_converted = last_updated_converted
        self.price_cny = price_cny
        self.volume_24h_cny = volume_24h_cny
        self.market_cap_cny = market_cap_cny

# Create tables
# TODO: Implement table existence check. If a table already exists, no need to create. 
Base.metadata.create_all(engine)