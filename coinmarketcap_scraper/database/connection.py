# -*- coding: utf-8 -*-
# @Author: panc25
# @Date:   2018-01-14 17:29:23
# @Last Modified by:   panc25
# @Last Modified time: 2018-01-14 22:44:00


'''Basic objects to facilitate database connection.

[description]
'''


from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker 

# db settings 
dbuser = 
dbpass = 
dbhost = 
dbname = 

engine = create_engine("mysql://{user_name}:{password}@{host_port}/{db_name}?charset=utf8&use_unicode=0".format(user_name=dbuser,
                                                                                                                password=dbpass,
                                                                                                                host_port=dbhost,
                                                                                                                db_name=dbname))

Session = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=False,
                                 bind=engine))

# The declarative_base() callable returns a new base class 
# from which all mapped classes should inherit
Base = declarative_base()
