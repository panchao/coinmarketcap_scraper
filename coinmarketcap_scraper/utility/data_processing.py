# -*- coding: utf-8 -*-
# @Author: panc25
# @Date:   2018-01-14 17:21:48
# @Last Modified by:   panc25
# @Last Modified time: 2018-01-14 17:27:19


from datetime import datetime


CURRENCY_LIST = [
    "AUD",
    "BRL",
    "CAD",
    "CHF",
    "CLP",
    "CNY",
    "CZK",
    "DKK",
    "EUR",
    "GBP",
    "HKD",
    "HUF",
    "IDR",
    "ILS",
    "INR",
    "JPY",
    "KRW",
    "MXN",
    "MYR",
    "NOK",
    "NZD",
    "PHP",
    "PKR",
    "PLN",
    "RUB",
    "SEK",
    "SGD",
    "THB",
    "TRY",
    "TWD",
    "ZAR"
]


def convertEpochTimeToDatetime(epoch_time):
    """converting epoch time with milliseconds to datetime
    
    Parameter
    ---------
    epoch_time : int
        epoch time with milliseconds
    """
    s = epoch_time / 1000.
    return datetime.fromtimestamp(s)
